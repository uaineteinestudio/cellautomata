# CellAutomata

A C# CellAutomata class for creating cave systems and the like. Adjustable dimensions.

## Getting Started

This is a static class so simply call it via:
```csharp
bool[,] map = CellAutomata.CellMap(
            int Width, int ChunkWidth, int Height, int cellAutomaDepth, float chanceToStartAlive,
            int simulationSteps, int deathLimit, int birthLimit)
```

* Note the class is under the namespace CA

## Authors

* **Daniel Stamer-Squair** - *UaineTeine*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details